for env in ci sit uat prd; do
  dir="./ssh-keys/$env"
  file="$dir/id_rsa"
  mkdir -p $dir
  if [[ ! -f "$file" ]]; then
    ssh-keygen -C "u$env@$env" -f "$file" -N ""
  fi
done
