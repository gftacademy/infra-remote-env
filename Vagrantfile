Vagrant.configure("2") do |config|

    config.vm.box = "ubuntu/xenial64"

    boxes = {
        "ci" => { :ip => "192.168.33.10", :user => "uci"},
        "sit" => { :ip => "192.168.33.11", :user => "usit"},
        "uat" => { :ip => "192.168.33.12", :user => "uuat"},
        "prd" => { :ip => "192.168.33.13", :user => "uprd"}
    }

    boxes.each do |key, value|
        config.vm.define "#{key}" do |node|
            node.vm.hostname = "#{key}"
            node.vm.network "private_network", ip: "#{value[:ip]}"
            node.vm.provision :shell, path: "./install-software.sh"
            node.vm.provision :shell, inline: "useradd -m -s /bin/bash -U #{value[:user]} -u 666 --groups vagrant; echo \"%#{value[:user]} ALL=(ALL) NOPASSWD: ALL\" > /etc/sudoers.d/#{value[:user]}"
            node.vm.provision :shell, inline: "echo ENV_NAME=#{key} >> /etc/environment"
            node.vm.provision :file, source: "./ssh-keys/#{key}/id_rsa", destination: "/tmp/id_rsa"
            node.vm.provision :file, source: "./ssh-keys/#{key}/id_rsa.pub", destination: "/tmp/id_rsa.pub"
            node.vm.provision :file, source: "./ssh-keys/ci/id_rsa.pub", destination: "/tmp/authorized_keys"

            node.vm.provision :shell, inline: "mkdir /home/#{value[:user]}/.ssh"
            node.vm.provision :shell, inline: "mv /tmp/id_rsa /home/#{value[:user]}/.ssh/id_rsa"
            node.vm.provision :shell, inline: "mv /tmp/id_rsa.pub /home/#{value[:user]}/.ssh/id_rsa.pub"
            node.vm.provision :shell, inline: "mv /tmp/authorized_keys /home/#{value[:user]}/.ssh/authorized_keys"
            node.vm.provision :shell, inline: "chown -R #{value[:user]}:#{value[:user]} /home/#{value[:user]}"
            node.vm.provision :shell, inline: "chmod 700 /home/#{value[:user]}/.ssh; chmod 600 /home/#{value[:user]}/.ssh/*; chmod 644 /home/#{value[:user]}/.ssh/id_rsa.pub"


            node.vm.provider "virtualbox" do |vb|
                vb.name = "prv-gft-academy-#{key}"
                vb.memory = "1024"
            end
        end
    end

    config.vm.define "ci" do |ci|
        ci.vm.provision :shell, path: "./ci/install-ansible.sh"
        ci.vm.provision :file, source: "./ci/deploy.yml", destination: "/tmp/ansible/deploy.yml"
        ci.vm.provision :file, source: "./ci/inventory.yml", destination: "/tmp/ansible/inventory.yml"
        ci.vm.provision :file, source: "./ci/deploy.sh", destination: "/tmp/ansible/deploy.sh"
        ci.vm.provision :file, source: "./ci/start.sh.j2", destination: "/tmp/ansible/start.sh.j2"
        ci.vm.provision :file, source: "./ci/stop.sh.j2", destination: "/tmp/ansible/stop.sh.j2"
        ci.vm.provision :shell, inline: "mv /tmp/ansible/* /home/uci"
        ci.vm.provision :shell, inline: "chown -R uci:uci /home/uci"
        ci.vm.provision :shell, inline: "chmod u+x /home/uci/deploy.sh"
    end

end
