#!/bin/bash
if [[ ! $# -eq 3  ]] && [[ ! $# -eq 4 ]]; then
  echo "Usage: ./deploy.sh sit|uat|prd [groupId] artifactId version"
  exit 1
fi

case $1 in
  "sit") echo "deploying $2:$3 to sit env" ;;
  "sit") echo "deploying $2:$3 to uat env" ;;
  "sit") echo "deploying $2:$3 to prd env" ;;
  *) echo "wrong environment"; exit 1
esac

if [[ $# -eq 3  ]]; then
  groupId="pl.gftacademy.micro"
  artifactId=$2
  version=$3
else
  groupId=$2
  artifactId=$3
  version=$4
fi

ansible-playbook deploy.yml --inventory inventory.yml --extra-vars "host=$1 user=u$1 groupId=$groupId artifactId=$artifactId version=$version"
