apt-get update
apt-get install -y unzip jq
apt-get install -y python-minimal python-setuptools
apt-get install -y openjdk-8-jdk
curl -sL https://deb.nodesource.com/setup_11.x | bash -
apt-get install -y nodejs
